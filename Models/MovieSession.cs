﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class MovieSession
    {
        public MovieSession()
        {
            MovieSessionEpisode = new HashSet<MovieSessionEpisode>();
        }

        public int Id { get; set; }
        public int? MovieId { get; set; }
        public string Name { get; set; }

        public virtual Movie Movie { get; set; }
        public virtual ICollection<MovieSessionEpisode> MovieSessionEpisode { get; set; }
    }
}
