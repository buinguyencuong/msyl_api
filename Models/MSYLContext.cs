﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MSYL_API.Models
{
    public partial class MSYLContext : DbContext
    {
        public MSYLContext()
        {
        }

        public MSYLContext(DbContextOptions<MSYLContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Gender> Gender { get; set; }
        public virtual DbSet<Genre> Genre { get; set; }
        public virtual DbSet<Keyword> Keyword { get; set; }
        public virtual DbSet<Language> Language { get; set; }
        public virtual DbSet<Movie> Movie { get; set; }
        public virtual DbSet<MovieCast> MovieCast { get; set; }
        public virtual DbSet<MovieCategory> MovieCategory { get; set; }
        public virtual DbSet<MovieCompany> MovieCompany { get; set; }
        public virtual DbSet<MovieCountry> MovieCountry { get; set; }
        public virtual DbSet<MovieCrew> MovieCrew { get; set; }
        public virtual DbSet<MovieGenre> MovieGenre { get; set; }
        public virtual DbSet<MovieKeyword> MovieKeyword { get; set; }
        public virtual DbSet<MovieLanguage> MovieLanguage { get; set; }
        public virtual DbSet<MovieSession> MovieSession { get; set; }
        public virtual DbSet<MovieSessionEpisode> MovieSessionEpisode { get; set; }
        public virtual DbSet<MovieSessionEpisodeSource> MovieSessionEpisodeSource { get; set; }
        public virtual DbSet<Person> Person { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Gender>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Genre>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Keyword>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code).HasColumnName("code");

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<Movie>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
            });

            modelBuilder.Entity<MovieCast>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.CastOrder).HasColumnName("cast_order");

                entity.Property(e => e.GenderId).HasColumnName("gender_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.HasOne(d => d.Gender)
                    .WithMany()
                    .HasForeignKey(d => d.GenderId)
                    .HasConstraintName("FK_MovieCast_Gender");

                entity.HasOne(d => d.Movie)
                    .WithMany()
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_MovieCast_Movie");
            });

            modelBuilder.Entity<MovieCategory>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<MovieCompany>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.CompanyId).HasColumnName("company_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.HasOne(d => d.Company)
                    .WithMany()
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_MovieCompany_Company");

                entity.HasOne(d => d.Movie)
                    .WithMany()
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_MovieCompany_Movie");
            });

            modelBuilder.Entity<MovieCountry>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.CountryId).HasColumnName("country_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.HasOne(d => d.Country)
                    .WithMany()
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_MovieCountry_Country");

                entity.HasOne(d => d.Movie)
                    .WithMany()
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_MovieCountry_Movie");
            });

            modelBuilder.Entity<MovieCrew>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.DepartmentId).HasColumnName("department_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.PersonId).HasColumnName("person_id");

                entity.HasOne(d => d.Department)
                    .WithMany()
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK_MovieCrew_Department");

                entity.HasOne(d => d.Movie)
                    .WithMany()
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_MovieCrew_Gender");

                entity.HasOne(d => d.Person)
                    .WithMany()
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_MovieCrew_Person");
            });

            modelBuilder.Entity<MovieGenre>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.GenreId).HasColumnName("genre_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.HasOne(d => d.Genre)
                    .WithMany()
                    .HasForeignKey(d => d.GenreId)
                    .HasConstraintName("FK_MovieGenre_Genre");

                entity.HasOne(d => d.Movie)
                    .WithMany()
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_MovieGenre_Movie");
            });

            modelBuilder.Entity<MovieKeyword>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.KeywordId).HasColumnName("keyword_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.HasOne(d => d.Keyword)
                    .WithMany()
                    .HasForeignKey(d => d.KeywordId)
                    .HasConstraintName("FK_MovieKeyword_Keyword");

                entity.HasOne(d => d.Movie)
                    .WithMany()
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_MovieKeyword_Movie");
            });

            modelBuilder.Entity<MovieLanguage>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.LanguageId).HasColumnName("language_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.HasOne(d => d.Language)
                    .WithMany()
                    .HasForeignKey(d => d.LanguageId)
                    .HasConstraintName("FK_MovieLanguage_Language");

                entity.HasOne(d => d.Movie)
                    .WithMany()
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_MovieLanguage_Movie");
            });

            modelBuilder.Entity<MovieSession>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieSession)
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("FK_MovieSession_Movie");
            });

            modelBuilder.Entity<MovieSessionEpisode>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.MovieSessionId).HasColumnName("movie_session_id");

                entity.HasOne(d => d.MovieSession)
                    .WithMany(p => p.MovieSessionEpisode)
                    .HasForeignKey(d => d.MovieSessionId)
                    .HasConstraintName("FK_MovieSessionEpisode_Session");
            });

            modelBuilder.Entity<MovieSessionEpisodeSource>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.MovieSessionEpisodeId).HasColumnName("movie_session_episode_id");

                entity.HasOne(d => d.MovieSessionEpisode)
                    .WithMany(p => p.MovieSessionEpisodeSource)
                    .HasForeignKey(d => d.MovieSessionEpisodeId)
                    .HasConstraintName("FK_MovieEpisodeSource_Episode");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name).HasColumnName("name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
