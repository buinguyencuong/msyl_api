﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class MovieEpisode
    {
        public MovieEpisode()
        {
            MovieSessionEpisodeSource = new HashSet<MovieSessionEpisodeSource>();
        }

        public int Id { get; set; }
        public int? MovieSessionId { get; set; }
        public string Name { get; set; }

        public virtual MovieSession MovieSession { get; set; }
        public virtual ICollection<MovieSessionEpisodeSource> MovieSessionEpisodeSource { get; set; }
    }
}
