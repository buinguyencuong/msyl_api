﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class MovieLanguage
    {
        public int? MovieId { get; set; }
        public int? LanguageId { get; set; }

        public virtual Language Language { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
