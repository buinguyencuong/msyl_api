﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class MovieSessionEpisodeSource
    {
        public int Id { get; set; }
        public int? MovieSessionEpisodeId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Source { get; set; }

        public virtual MovieSessionEpisode MovieSessionEpisode { get; set; }
    }
}
