﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class MovieCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
}
