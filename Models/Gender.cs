﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class Gender
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
