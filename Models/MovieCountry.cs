﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class MovieCountry
    {
        public int? MovieId { get; set; }
        public int? CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
