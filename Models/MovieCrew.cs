﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class MovieCrew
    {
        public int? PersonId { get; set; }
        public int? MovieId { get; set; }
        public int? DepartmentId { get; set; }
        public string Name { get; set; }

        public virtual Department Department { get; set; }
        public virtual Movie Movie { get; set; }
        public virtual Person Person { get; set; }
    }
}
