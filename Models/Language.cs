﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class Language
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
