﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class MovieCast
    {
        public int? MovieId { get; set; }
        public int? GenderId { get; set; }
        public string Name { get; set; }
        public string CastOrder { get; set; }

        public virtual Gender Gender { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
