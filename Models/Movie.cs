﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class Movie
    {
        public Movie()
        {
            MovieSession = new HashSet<MovieSession>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public string Banner { get; set; }

        public virtual ICollection<MovieSession> MovieSession { get; set; }
    }
}
