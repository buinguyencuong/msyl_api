﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
