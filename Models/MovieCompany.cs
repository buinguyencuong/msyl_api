﻿using System;
using System.Collections.Generic;

namespace MSYL_API.Models
{
    public partial class MovieCompany
    {
        public int? MovieId { get; set; }
        public int? CompanyId { get; set; }

        public virtual Company Company { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
