﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MSYL_API.Models;

namespace MSYL_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieSessionEpisodeSourcesController : ControllerBase
    {
        private readonly MSYLContext _context;

        public MovieSessionEpisodeSourcesController(MSYLContext context)
        {
            _context = context;
        }

        // GET: api/MovieSessionEpisodeSources
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieSessionEpisodeSource>>> GetMovieSessionEpisodeSource()
        {
            return await _context.MovieSessionEpisodeSource.ToListAsync();
        }

        // GET: api/MovieSessionEpisodeSources/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieSessionEpisodeSource>> GetMovieSessionEpisodeSource(int id)
        {
            var movieSessionEpisodeSource = await _context.MovieSessionEpisodeSource.FindAsync(id);

            if (movieSessionEpisodeSource == null)
            {
                return NotFound();
            }

            return movieSessionEpisodeSource;
        }

        // PUT: api/MovieSessionEpisodeSources/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieSessionEpisodeSource(int id, MovieSessionEpisodeSource movieSessionEpisodeSource)
        {
            if (id != movieSessionEpisodeSource.Id)
            {
                return BadRequest();
            }

            _context.Entry(movieSessionEpisodeSource).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieSessionEpisodeSourceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovieSessionEpisodeSources
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieSessionEpisodeSource>> PostMovieSessionEpisodeSource(MovieSessionEpisodeSource movieSessionEpisodeSource)
        {
            _context.MovieSessionEpisodeSource.Add(movieSessionEpisodeSource);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovieSessionEpisodeSource", new { id = movieSessionEpisodeSource.Id }, movieSessionEpisodeSource);
        }

        // DELETE: api/MovieSessionEpisodeSources/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieSessionEpisodeSource>> DeleteMovieSessionEpisodeSource(int id)
        {
            var movieSessionEpisodeSource = await _context.MovieSessionEpisodeSource.FindAsync(id);
            if (movieSessionEpisodeSource == null)
            {
                return NotFound();
            }

            _context.MovieSessionEpisodeSource.Remove(movieSessionEpisodeSource);
            await _context.SaveChangesAsync();

            return movieSessionEpisodeSource;
        }

        private bool MovieSessionEpisodeSourceExists(int id)
        {
            return _context.MovieSessionEpisodeSource.Any(e => e.Id == id);
        }
    }
}
