﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MSYL_API.Models;

namespace MSYL_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieSessionsController : ControllerBase
    {
        private readonly MSYLContext _context;

        public MovieSessionsController(MSYLContext context)
        {
            _context = context;
        }

        // GET: api/MovieSessions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieSession>>> GetMovieSession()
        {
            return await _context.MovieSession.ToListAsync();
        }

        // GET: api/MovieSessions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieSession>> GetMovieSession(int id)
        {
            var movieSession = await _context.MovieSession.FindAsync(id);

            if (movieSession == null)
            {
                return NotFound();
            }

            return movieSession;
        }

        // PUT: api/MovieSessions/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieSession(int id, MovieSession movieSession)
        {
            if (id != movieSession.Id)
            {
                return BadRequest();
            }

            _context.Entry(movieSession).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieSessionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovieSessions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieSession>> PostMovieSession(MovieSession movieSession)
        {
            _context.MovieSession.Add(movieSession);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovieSession", new { id = movieSession.Id }, movieSession);
        }

        // DELETE: api/MovieSessions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieSession>> DeleteMovieSession(int id)
        {
            var movieSession = await _context.MovieSession.FindAsync(id);
            if (movieSession == null)
            {
                return NotFound();
            }

            _context.MovieSession.Remove(movieSession);
            await _context.SaveChangesAsync();

            return movieSession;
        }

        private bool MovieSessionExists(int id)
        {
            return _context.MovieSession.Any(e => e.Id == id);
        }
    }
}
