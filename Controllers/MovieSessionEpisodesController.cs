﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MSYL_API.Models;

namespace MSYL_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieSessionEpisodesController : ControllerBase
    {
        private readonly MSYLContext _context;

        public MovieSessionEpisodesController(MSYLContext context)
        {
            _context = context;
        }

        // GET: api/MovieSessionEpisodes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieSessionEpisode>>> GetMovieSessionEpisode()
        {
            return await _context.MovieSessionEpisode.ToListAsync();
        }

        // GET: api/MovieSessionEpisodes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieSessionEpisode>> GetMovieSessionEpisode(int id)
        {
            var movieSessionEpisode = await _context.MovieSessionEpisode.FindAsync(id);

            if (movieSessionEpisode == null)
            {
                return NotFound();
            }

            return movieSessionEpisode;
        }

        // PUT: api/MovieSessionEpisodes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieSessionEpisode(int id, MovieSessionEpisode movieSessionEpisode)
        {
            if (id != movieSessionEpisode.Id)
            {
                return BadRequest();
            }

            _context.Entry(movieSessionEpisode).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieSessionEpisodeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MovieSessionEpisodes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieSessionEpisode>> PostMovieSessionEpisode(MovieSessionEpisode movieSessionEpisode)
        {
            _context.MovieSessionEpisode.Add(movieSessionEpisode);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovieSessionEpisode", new { id = movieSessionEpisode.Id }, movieSessionEpisode);
        }

        // DELETE: api/MovieSessionEpisodes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieSessionEpisode>> DeleteMovieSessionEpisode(int id)
        {
            var movieSessionEpisode = await _context.MovieSessionEpisode.FindAsync(id);
            if (movieSessionEpisode == null)
            {
                return NotFound();
            }

            _context.MovieSessionEpisode.Remove(movieSessionEpisode);
            await _context.SaveChangesAsync();

            return movieSessionEpisode;
        }

        private bool MovieSessionEpisodeExists(int id)
        {
            return _context.MovieSessionEpisode.Any(e => e.Id == id);
        }
    }
}
